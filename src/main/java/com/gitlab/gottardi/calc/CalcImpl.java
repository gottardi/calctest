package com.gitlab.gottardi.calc;

public final class CalcImpl implements Calc {

	public int add(int a, int b) {		
		return (a + b);
	}

	public int sub(int a, int b) {
		return (a - b);
	}

	public int mul(int a, int b) {
		return (a * b);
	}

	public int div(int a, int b) throws ArithmeticException {
		if (b == 0)	{
			throw new ArithmeticException();
		}
		else {
			return (a / b);
		}
	}

}
