package com.gitlab.gottardi.calc;

import java.util.Scanner;

public final class CalcMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Calc calc = new CalcImpl();
		
		System.out.println("Entre com dois números inteiros.");
		int a = scanner.nextInt();
		int b = scanner.nextInt();
		
		System.out.println("Soma: " + 			calc.add(a,b));
		System.out.println("Subtração: " + 		calc.sub(a,b));
		System.out.println("Multiplicação: " +	calc.mul(a,b));
		try	{
			System.out.println("Divisão: " + 		calc.div(a,b));
		} catch (ArithmeticException ae) {
			System.err.println("Erro: divisão por zero.");
		}
		
		scanner.close();
	}

}
