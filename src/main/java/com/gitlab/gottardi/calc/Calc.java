package com.gitlab.gottardi.calc;

import java.lang.ArithmeticException;

public interface Calc {
	public int add(final int a, final int b);
	public int sub(final int a, final int b);
	public int mul(final int a, final int b);
	public int div(final int a, final int b) throws ArithmeticException;	
}
