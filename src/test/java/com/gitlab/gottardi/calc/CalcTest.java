package com.gitlab.gottardi.calc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

public class CalcTest {
	
	Calc calc;

	@Before
	public void setUp() throws Exception {
		calc = new CalcImpl();
	}

	@Test
	public void test() {
		 assertEquals(2, calc.add(1, 1));
		 assertEquals(4, calc.add(2, 2));
		 assertEquals(0, calc.sub(1, 1));
		 assertEquals(0, calc.sub(2, 2));
		 assertEquals(1, calc.mul(1, 1));
		 assertEquals(4, calc.mul(2, 2));
		 assertEquals(1, calc.div(1, 1));
		 assertEquals(1, calc.div(2, 2));
		 
		 try
		 {
			 calc.div(2, 0);
			 fail("Falhou ao lançar exceção.");
		 } catch (Throwable ae) {
			 assertTrue(ae instanceof ArithmeticException);		 
		 }
		 
	}

}
